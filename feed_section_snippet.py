
m abc import ABCMeta

from braces.views import JSONResponseMixin

from core.constants import FEED_ENTRIES_DEFAULT_LIMIT
from core.utils.mixins import SerializerBaseMixin

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from .mixins import (
    FeedChannelsMixin,
    FeedViewOffsetMixin,
)

from .serializers import (
    FeedChannelsViewSerializer,
    FeedViewGetSerializer,
    CreateFeedEntryPostSerializer,
    DeleteFeedEntryViewDeleteSerializer,
    LikeDislikeFeedEntryViewPostSerializer,
    LikeDislikeEntryFeedSerializer,
    EditFeedEntryViewPutSerializer,
    ValidateChallengeForFeedEViewGetSerializer
)

from .models import FeedEntry, FeedEntryLike


class FeedChannelsView(SerializerBaseMixin, FeedChannelsMixin, JSONResponseMixin, APIView):
    """
    View to return posts/comments/replies &
    show feed section as a part of a dashboard
    """
    serializers = True
    serializers_map = {
        "FeedEntryDynamicSerializer": FeedChannelsViewSerializer
    }

    def get(self, request, *args, **kwargs):
        json_resp = {'success': False, 'data': None}
        resp_status = status.HTTP_400_BAD_REQUEST

        self.init_get(request, *args, **kwargs)

        if self.params.get('_errors', None):
            json_resp['error'] = self.params['_errors']
            return self.render_json_response(json_resp, status=resp_status)

        challenge_id = self.params.get("challenge_id", None)
        channels = self.get_channels(request, challenge_id, *args, **kwargs)

        if channels:
            json_resp = {'success': True, 'data': channels}
            resp_status = status.HTTP_200_OK

        return self.render_json_response(json_resp, status=resp_status)


class FeedView(SerializerBaseMixin, FeedViewOffsetMixin, JSONResponseMixin, APIView):
    serializers = True
    serializers_map = {
        "FeedEntryDynamicSerializer": FeedViewGetSerializer
    }

    def get(self, request, *args, **kwargs):

        self.init_get(request, *args, **kwargs)
        json_resp = {'success': False, 'data': None}
        resp_status = status.HTTP_400_BAD_REQUEST

        user = request.user
        if self.params.get('_errors', None):
            json_resp['error'] = self.params['_errors']
            return self.render_json_response(json_resp, status=resp_status)

        channel, challenge_id, limit, offset = \
            self.params.get("channel", None), self.params.get("challenge_id", None), \
            self.params.get("limit", FEED_ENTRIES_DEFAULT_LIMIT), self.params.get("offset", 0)

        feed_entries = FeedEntry.objects.filter(
            entry_type="post",
            challenge__id=challenge_id,
            channel=channel
        )

        self.slice(feed_entries, limit, offset)

        response = FeedViewGetSerializer(
            self.queryset,
            context={'request': request, 'channel': channel},
            many=True
        ).data

        json_resp = {'success': True, 'data': response}
        resp_status = status.HTTP_200_OK

        return self.render_json_response(json_resp, status=resp_status)


class CreateFeedEntryMeta:
    __metaclass__ = ABCMeta

    def __init__(self, entry_type, channel, participant, challenge, content=None, parent=None, image=None, errors=None):
        self.entry_type = entry_type
        self.channel = channel
        self.participant = participant
        self.challenge = challenge
        self.content = content
        self.parent = parent
        self.image = image
        self.errors = errors


class CreateFeedEntry(CreateFeedEntryMeta, FeedView):

    def validate_data(self, **kwargs):

        entry_types_with_parent_object = ["comment", "reply"]
        entry_type_without_parent_object = ["post"]

        if kwargs.get("entry_type", None) in entry_types_with_parent_object and not kwargs.get("parent", None):
            return False, "No parent ID was specified for comments/replies entry type"

        if kwargs.get("entry_type", None) in entry_type_without_parent_object and kwargs.get("parent", None):
            return False, "Parent ID was specified for post entry type"

        return True, ""

    @staticmethod
    def get_creator(**kwargs):

        if kwargs.get("entry_type", None):
            creators = {
                "post": CreateFeedEntryPost(**kwargs),
                "comment": CreateFeedEntryComment(**kwargs),
                "reply": CreateFeedEntryReply(**kwargs)
            }
            return creators.get(kwargs.get("entry_type"))
        return None


class CreateFeedEntryPost(CreateFeedEntry):
    def create_feed_entry(self):
        if not self.errors:
            return FeedEntry.objects.create(
                channel=self.channel,
                entry_type=self.entry_type,
                participant=self.participant,
                challenge=self.challenge,
                content=self.content,
                image=self.image
            )
        return False


class CreateFeedEntryComment(CreateFeedEntry):
    def create_feed_entry(self):
        if not self.errors:
            return FeedEntry.objects.create(
                channel=self.channel,
                entry_type=self.entry_type,
                participant=self.participant,
                challenge=self.challenge,
                parent=self.parent,
                content=self.content,
                image=self.image
            )
        return False


class CreateFeedEntryReply(CreateFeedEntryComment):
    def create_feed_entry(self):
        return super().create_feed_entry()


class CreateFeedEntryView(SerializerBaseMixin, JSONResponseMixin, APIView):
    serializers = True
    serializers_map = {"CreateFeedEntryViewPostSerializer": CreateFeedEntryPostSerializer}

    def post(self, request, *args, **kwargs):

        self.init_post(request, *args, **kwargs)
        json_resp = {'success': False, 'data': None, 'details': ""}
        resp_status = status.HTTP_400_BAD_REQUEST

        if self.params.get('_errors', None):
            json_resp['error'] = self.params['_errors']
            return self.render_json_response(json_resp, status=resp_status)

        creator = CreateFeedEntry.get_creator(**self.params)
        is_valid, message = creator.validate_data(**self.params)
        created_feed_entry = creator.create_feed_entry()

        if is_valid and created_feed_entry:
            json_resp = {'success': True}
            json_resp.update(FeedViewGetSerializer(created_feed_entry, many=False).data)
            resp_status = status.HTTP_200_OK

            return self.render_json_response(json_resp, status=resp_status)

        json_resp.update({"details": message})
        return self.render_json_response(json_resp, status=resp_status)


class DeleteFeedEntryView(SerializerBaseMixin, JSONResponseMixin, APIView):
    serializers = True
    serializers_map = {"DeleteFeedEntryViewDeleteSerializer": DeleteFeedEntryViewDeleteSerializer}

    def delete(self, request, *args, **kwargs):
        self.init_delete(request, *args, **kwargs)
        json_resp = {'success': False, 'data': None, 'details': ""}
        resp_status = status.HTTP_400_BAD_REQUEST

        if self.params.get('_errors', None):
            json_resp['error'] = self.params['_errors']
            return self.render_json_response(json_resp, status=resp_status)

        feed_entry_to_delete = self.params.get("id", None)
        if feed_entry_to_delete:
            feed_entry_to_delete.delete()

            json_resp = {'success': True}
            resp_status = status.HTTP_200_OK

            return self.render_json_response(json_resp, status=resp_status)

        return self.render_json_response(json_resp, status=resp_status)


class LikeDislikeFeedEntryView(SerializerBaseMixin, JSONResponseMixin, APIView):
    serializers = True
    serializers_map = {"LikeDislikeFeedEntryViewPostSerializer": LikeDislikeFeedEntryViewPostSerializer}

    def post(self, request, *args, **kwargs):
        self.init_post(request, *args, **kwargs)
        json_resp = {'success': False, 'data': None, 'details': "", "likes": ""}
        resp_status = status.HTTP_400_BAD_REQUEST

        if self.params.get('_errors', None):
            json_resp['error'] = self.params['_errors']
            return self.render_json_response(json_resp, status=resp_status)

        user = request.user
        feed_entry_to_like = self.params.get("id", None)

        if not FeedEntryLike.objects.filter(participant=user, feed_entry=feed_entry_to_like).exists():
            FeedEntryLike.objects.create(
                feed_entry=feed_entry_to_like,
                participant=user
            )

            json_resp = {'success': True}
            json_resp.update({"details": "Record has been liked"})
            resp_status = status.HTTP_200_OK

            json_resp.update(LikeDislikeEntryFeedSerializer(feed_entry_to_like, many=False).data)
            return self.render_json_response(json_resp, status=resp_status)

        else:
            FeedEntryLike.objects.get(participant=user, feed_entry=feed_entry_to_like).delete()

            json_resp = {'success': True}
            json_resp.update({"details": "Like record has been removed"})
            resp_status = status.HTTP_200_OK

            json_resp.update(LikeDislikeEntryFeedSerializer(feed_entry_to_like, many=False).data)
            return self.render_json_response(json_resp, status=resp_status)


class EditFeedEntryView(SerializerBaseMixin, JSONResponseMixin, APIView):
    serializers = True
    serializers_map = {"EditFeedEntryViewPutSerializer": EditFeedEntryViewPutSerializer}

    def put(self, request, *args, **kwargs):
        self.init_put(request, *args, **kwargs)
        json_resp = {'success': False, 'data': None, 'details': ""}
        resp_status = status.HTTP_400_BAD_REQUEST

        if self.params.get('_errors', None):
            json_resp['error'] = self.params['_errors']
            return self.render_json_response(json_resp, status=resp_status)

        feed_entry_to_edit = self.params.get("id", None)

        feed_entry_to_edit.content = self.params.get("content")\
            if self.params.get("content", None) else feed_entry_to_edit.content

        feed_entry_to_edit.image = self.params.get("image") \
            if self.params.get("image", None) else feed_entry_to_edit.image

        feed_entry_to_edit.save()

        json_resp = {'success': True}
        json_resp.update(FeedViewGetSerializer(feed_entry_to_edit,
                                               context={'parse_nested': False}, many=False).data)
        resp_status = status.HTTP_200_OK

        return self.render_json_response(json_resp, status=resp_status)


class ValidateChallengeForFeedEView(SerializerBaseMixin, JSONResponseMixin, APIView):
    serializers = True
    serializers_map = {"ValidateChallengeForFeedEViewGetSerializer": ValidateChallengeForFeedEViewGetSerializer}

    def get(self, request, *args, **kwargs):
        self.init_get(request, *args, **kwargs)
        json_resp = {'success': False, 'data': None}
        resp_status = status.HTTP_400_BAD_REQUEST

        if self.params.get('_errors', None):
            json_resp['error'] = self.params['_errors']
            return self.render_json_response(json_resp, status=resp_status)

        json_resp = {'success': True}
        resp_status = status.HTTP_200_OK
        return self.render_json_response(json_resp, status=resp_status)

